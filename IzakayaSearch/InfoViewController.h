//
//  InfoViewController.h
//  IzakayaSearch
//
//  Created by TokugawaRyuichi on 1/12/15.
//  Copyright (c) 2015 HujiToku. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InfoViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *returnBtn;
- (IBAction)returnBtnTapped:(id)sender;
- (IBAction)HotpepperBtnTapped:(id)sender;
- (IBAction)ryukorinTapped:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *ryukorinBtn;

@end
