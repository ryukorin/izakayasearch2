//
//  PinAnnotationView.h
//  IzakayaSearch
//
//  Created by TokugawaRyuichi on 11/21/14.
//  Copyright (c) 2014 HujiToku. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface PinAnnotationView : MKAnnotationView

@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property NSInteger number;


@end
