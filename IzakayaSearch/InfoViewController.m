//
//  InfoViewController.m
//  IzakayaSearch
//
//  Created by TokugawaRyuichi on 1/12/15.
//  Copyright (c) 2015 HujiToku. All rights reserved.
//

#import "InfoViewController.h"

@interface InfoViewController ()
{
    UIWebView *_webView;
}

@end

@implementation InfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.2];
    UIVisualEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffectView *effectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    effectView.frame = self.view.bounds;
    [self.view addSubview:effectView];
    [self.view sendSubviewToBack:effectView];
    
    //ryukorinBtn
    [_ryukorinBtn setEnabled:NO];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIModalPresentationStyle)modalPresentationStyle
{
    return UIModalPresentationOverCurrentContext;
}

- (IBAction)returnBtnTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)HotpepperBtnTapped:(id)sender {
    _webView = [[UIWebView alloc]initWithFrame:self.view.bounds];
    NSURL * url = [NSURL URLWithString:@"http://webservice.recruit.co.jp/"];
    NSURLRequest *req = [NSURLRequest requestWithURL:url];
    [_webView loadRequest:req];
    [self.view addSubview:_webView];
    
    CGFloat windowWidth = _webView.frame.size.width;
    CGFloat windowHeight = _webView.frame.size.height;
    CGRect rect = CGRectMake(windowWidth/2 - 100/2, windowHeight/3*2 - 40/2, 100, 40);
    UIButton *returnBtn = [[UIButton alloc]initWithFrame:rect];
    [returnBtn setTitle:@"戻る" forState:UIControlStateNormal];
    [returnBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [returnBtn addTarget:nil action:@selector(webViewReturnBtnTapped) forControlEvents:UIControlEventTouchUpInside];
    [_webView addSubview:returnBtn];
    [returnBtn setBackgroundColor:[UIColor yellowColor]];
}

- (IBAction)ryukorinTapped:(id)sender {
    
}

- (void)webViewReturnBtnTapped{
    [_webView removeFromSuperview];
}
@end
