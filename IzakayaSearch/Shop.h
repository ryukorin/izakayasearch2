//
//  Shop.h
//  IzakayaSearch
//
//  Created by TokugawaRyuichi on 11/19/14.
//  Copyright (c) 2014 HujiToku. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Shop : NSObject

@property (nonatomic, retain)NSString *name;
@property (nonatomic)float latitude;
@property (nonatomic)float longitude;


-(instancetype)initWithName:(NSString*)name Latitude:(double)latitude Longitude:(double)longitude;

@end
