//
//  ShopCollectionViewController.m
//  IzakayaSearch
//
//  Created by TokugawaRyuichi on 11/21/14.
//  Copyright (c) 2014 HujiToku. All rights reserved.
//

#import "ShopCollectionViewController.h"

@interface ShopCollectionViewController ()

@end

@implementation ShopCollectionViewController

@synthesize shopArray;

static NSString * const reuseIdentifier = @"ShopCells";

- (void)viewDidLoad {
    [super viewDidLoad];
//    Class shop = [ShopCell class];
////    [_shopCollectionView registerClass:shop forCellWithReuseIdentifier:reuseIdentifier];
//    UINib *nib = [UINib nibWithNibName:@"ShopCell" bundle:nil];
//    [_shopCollectionView registerNib:nib forCellWithReuseIdentifier:reuseIdentifier];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

/*
#pragma mark - Navigation


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (shopArray == nil || [shopArray count] == 0) {

        return 1;
    }
    else return [shopArray count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ShopCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    if (shopArray == nil) {
        #pragma mark 空セルを用意する
    }
    else{
        NSDictionary *shop = shopArray[indexPath.row];
        cell.nameLabel.text = [shop objectForKey:@"name"];
        NSDictionary *budget = [shop objectForKey:@"budget"];
        cell.bugetLabel.text = [budget objectForKey:@"average"];
    }
    
    return cell;
}



#pragma mark <UICollectionViewDelegate>


// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}


/*
// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/

/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/

@end
