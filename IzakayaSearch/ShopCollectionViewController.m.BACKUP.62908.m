//
//  ShopCollectionViewController.m
//  IzakayaSearch
//
//  Created by TokugawaRyuichi on 11/21/14.
//  Copyright (c) 2014 HujiToku. All rights reserved.
//

#import "ShopCollectionViewController.h"


@interface ShopCollectionViewController ()

@property MapController*mapcon;
@end

@implementation ShopCollectionViewController

@synthesize shopArray;
@synthesize mapcon;


static NSString * const reuseIdentifier = @"ShopCells";

- (void)viewDidLoad {
    [super viewDidLoad];
<<<<<<< HEAD
//    _shopCollectionView.pagingEnabled = YES;
=======
    _shopCollectionView.delegate = self;
    _shopCollectionView.dataSource = self;
    
//    self.refreshManager = [[MNMBottomPullToRefreshManager alloc] initWithPullToRefreshViewHeight:60.0 collectionView:_shopCollectionView withClient:self];
//   
>>>>>>> f54be86987e6368170fa8d598f86d4e902cb8214
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    //一番下までスクロールしたかどうか
    if(_shopCollectionView.contentOffset.x >= (_shopCollectionView.contentSize.width - _shopCollectionView.bounds.size.width))
    {
        //ここで次に表示する件数を取得して表示更新の処理を書けばOK
        MapController*mapcon=(MapController *)self.parentViewController;
        NSInteger reset=1;
        NSArray * newArray = [mapcon requestAndParseShopList:reset];
        for (NSDictionary *dic in newArray) {
            [mapcon.shopArray addObject:dic];
        }
        mapcon.i=0;
        [mapcon pinShops];
        [_shopCollectionView reloadData];
    }
}
/*
#pragma mark - Navigation


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (shopArray == nil || [shopArray count] == 0) {

        return 1;
    }
    else return [shopArray count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ShopCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    if (shopArray == nil) {
        #pragma mark 空セルを用意する
    }
    else{
        NSDictionary *shop = shopArray[indexPath.row];
        cell.nameLabel.text = [shop objectForKey:@"name"];
        NSDictionary *budget = [shop objectForKey:@"budget"];
        cell.bugetLabel.text = [budget objectForKey:@"average"];
        NSString *imageURLStr = [shop objectForKey:@"logo_image"];
        [cell.image setImageWithURL:[NSURL URLWithString:imageURLStr]
                       placeholderImage:[UIImage imageNamed:nil]];
    }
    
    return cell;
}



#pragma mark <UICollectionViewDelegate>


// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    NSArray *indexPaths = [_shopCollectionView indexPathsForVisibleItems];
    NSIndexPath *showedIndexPath = indexPaths[0];
    MapController *mapCon = (MapController *)self.parentViewController;
    
    NSDictionary *showedShop = mapCon.shopArray[showedIndexPath.row];
    NSString *latStr = [showedShop objectForKey:@"lat"];
    NSString *lngStr = [showedShop objectForKey:@"lng"];
    
    MKCoordinateRegion region = { { 0.0, 0.0 }, { 0.0, 0.0 } };
    region.center.latitude = latStr.doubleValue;
    region.center.longitude = lngStr.doubleValue;
    region.span.latitudeDelta = 0.007f;
    region.span.longitudeDelta = 0.007f;
    
    [mapCon.mapView setRegion:region animated:YES];
}


/*
// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/

/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/
//# pragma mark - MNMBottomPullToRefreshManager
//- (void)scrollViewDidScroll:(UIScrollView *)scrollView
//{
//    [self.refreshManager tableViewScrolled];
//}
//
//- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
//{
//    [self.refreshManager tableViewReleased];
//}
//
//- (void)bottomPullToRefreshTriggered:(MNMBottomPullToRefreshManager *)manager {
//    
//    [self performSelector:@selector(refresh) withObject:nil afterDelay:0.3f];
//}
//
//- (void)viewDidLayoutSubviews
//{
//    [super viewDidLayoutSubviews];
//    [self.refreshManager relocatePullToRefreshView];
//}
//- (void)refresh
//{
//    // データ更新 ...
//    
//    // 再レイアウト
//    [self.view layoutIfNeeded];
//    
//    
//    // 更新後に呼び出す
//    [self.refreshManager tableViewReloadFinished];
//}

@end
