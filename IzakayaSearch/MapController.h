//
//  MapController.h
//  IzakayaSearch
//
//  Created by TokugawaRyuichi on 11/13/14.
//  Copyright (c) 2014 HujiToku. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import <FontAwesome-iOS/NSString+FontAwesome.h>

#import "Shop.h"
#import "JsonParser.h"
#import "PinAnnotationView.h"
#import "PinAnnotation.h"
#import "ShopCollectionViewController.h"
#import "InfoViewController.h"


@interface MapController : UIViewController

//#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
//
{
    NSString *Pinidentifier;
}

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) NSMutableArray *shopArray;
@property (weak, nonatomic) IBOutlet UIView *container;
@property int i;
@property NSMutableArray *annotationArray;
@property PinAnnotationView *preSelectedPinView;
@property (weak, nonatomic) IBOutlet UIButton *infoBtn;


-(void)pinShops;
- (NSArray *)requestAndParseShopList:(NSInteger)reset;
- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(PinAnnotationView *)view;
-(void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(PinAnnotationView *)view;
- (IBAction)infoBtnTapped:(id)sender;





@end
