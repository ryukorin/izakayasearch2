//
//  AppDelegate.h
//  IzakayaSearch
//
//  Created by TokugawaRyuichi on 11/13/14.
//  Copyright (c) 2014 HujiToku. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

