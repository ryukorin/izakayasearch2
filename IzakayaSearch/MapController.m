//
//  MapController.m
//  IzakayaSearch
//
//  Created by TokugawaRyuichi on 11/13/14.
//  Copyright (c) 2014 HujiToku. All rights reserved.
//

#import "MapController.h"

@interface MapController () <MKMapViewDelegate, CLLocationManagerDelegate>
{
    CLLocation *_nowLocation;
    BOOL first_update_location;
    
}

@end

@implementation MapController
@synthesize locationManager;
@synthesize shopArray;
@synthesize annotationArray;
@synthesize preSelectedPinView;


- (void)viewDidLoad {
    first_update_location = YES;
    [super viewDidLoad];
    _i=0;
    [self initLocationManager];
    [self initMapView];
    _infoBtn.titleLabel.font = [UIFont fontWithName:@"FontAwesome" size:30];
    [_infoBtn setTitle:[NSString stringWithFormat:@"%@",[NSString awesomeIcon:FaInfo]] forState:UIControlStateNormal];
}


#pragma mark -
#pragma mark MapView

- (void)initMapView{
    //MKMapkit初期設定用
    _mapView.delegate = self;
    _mapView.showsUserLocation = YES;
}


- (void)pinShops{
    
    //アノテーション初期化
    [_mapView removeAnnotations:_mapView.annotations];
    annotationArray=[[NSMutableArray alloc]init];
    
    //ピンの位置情報をshopから取得、annotationArrayへ挿入
    if (shopArray != nil) {
       for (NSDictionary *shop in shopArray) {
        NSString *latStr = (NSString *)[shop objectForKey:@"lat"];
        float lat = latStr.floatValue;
        NSString *lngStr = (NSString *)[shop objectForKey:@"lng"];
        float lng = lngStr.floatValue;
        PinAnnotation *new = [[PinAnnotation alloc] initWithLocationCoordinate:CLLocationCoordinate2DMake(lat, lng) title:[shop objectForKey:@"name"] subtitle:nil];
        //価格帯ごとに割り振り
        NSString *cost = (NSString *)[[shop objectForKey:@"budget"]objectForKey:@"code"];
        if([cost isEqual:@"B001"]){
            new.cost=1;
        }else if([cost isEqual:@"B002"]||[cost isEqual:@"B003"]){
            new.cost=2;
        }else{
            new.cost=3;
        }
        //アクション用ピン識別ナンバー
        new.number=_i;
        _i++;
        
        NSLog(@"%dと%@、%d",new.number,new.title,new.cost);
        [annotationArray addObject:new];
       }
    }
    
    [_mapView addAnnotations:annotationArray];
    NSLog(@"%lu",(unsigned long)annotationArray.count);
}


-(MKAnnotationView*)mapView:(MKMapView*)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    
    //annotationはannotationArrayを指すだろう
    //現在地にピンはささない？
    if (annotation == _mapView.userLocation) {
        return nil;
    }else{

        //pin画像探し省略のため、サブクラスであるMKPinAnnotationクラス使用
        PinAnnotationView*annotationView;
        PinAnnotation *pinAnnotaion = (PinAnnotation *)annotation;
        annotationView = (PinAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:Pinidentifier];
        
        if(nil == annotationView) {
            annotationView = [[PinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:Pinidentifier];
        }
    
        NSLog(@"店番号%d",pinAnnotaion.number);
        
        annotationView.annotation = pinAnnotaion;
        if(pinAnnotaion.cost==1){
            annotationView.image=[UIImage imageNamed:@"PIN1"];
            annotationView.number=1;
        }else if(pinAnnotaion.cost==2){
            annotationView.image=[UIImage imageNamed:@"PIN2"];
            annotationView.number=2;
        }else if(pinAnnotaion.cost==3){
            annotationView.image=[UIImage imageNamed:@"PIN3"];
            annotationView.number=3;
        }
       
//        annotationView.canShowCallout = YES;  // この設定で吹き出しが出る
//        //ボタンの種類を指定（ここがないとタッチできない）
//        UIButton *detailButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
//        annotationView.rightCalloutAccessoryView = detailButton;
//    
        return annotationView;
    }
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(PinAnnotationView *)view {
    [self mapView:_mapView didDeselectAnnotationView:preSelectedPinView];
    
    PinAnnotation*pin=(PinAnnotation *)view.annotation;
    NSIndexPath *selectedIndexPath = [NSIndexPath indexPathForRow:pin.number inSection:0];
    ShopCollectionViewController *collectionCon = self.childViewControllers[0];
    if(view.number==1){
        view.image=[UIImage imageNamed:@"PIN1big"];
    }else if(view.number==2){
        view.image=[UIImage imageNamed:@"PIN2big"];
    }else if(view.number==3){
        view.image=[UIImage imageNamed:@"PIN3big"];
    }
    
    
    [collectionCon.shopCollectionView selectItemAtIndexPath:selectedIndexPath animated:YES scrollPosition:UICollectionViewScrollPositionLeft];
    preSelectedPinView = view;
}

-(void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(PinAnnotationView *)view{
    if(view.number==1){
        view.image=[UIImage imageNamed:@"PIN1"];
    }else if(view.number==2){
        view.image=[UIImage imageNamed:@"PIN2"];
    }else if(view.number==3){
        view.image=[UIImage imageNamed:@"PIN3"];
    }

}

- (IBAction)infoBtnTapped:(id)sender {
    InfoViewController *infoCon = [self.storyboard instantiateViewControllerWithIdentifier:@"info"];
    [self presentViewController:infoCon animated:YES completion:nil];
}

#pragma mark -
#pragma mark LocationManager

- (void)initLocationManager{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    
//    if(IS_OS_8_OR_LATER){
//        
//        [locationManager requestWhenInUseAuthorization];
//        [locationManager startUpdatingLocation];
//    }else{
//        [locationManager startUpdatingLocation];
//        
//    }
    
    if ([locationManager locationServicesEnabled] == NO) {
        UIAlertView*alertView=[[UIAlertView alloc]initWithTitle:nil message:@"GPSサービスをオンにしてください" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertView show];
        return;
    }
    
    if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        switch ([CLLocationManager authorizationStatus]) {
            case kCLAuthorizationStatusNotDetermined:
                [locationManager requestWhenInUseAuthorization];
                break;
            case kCLAuthorizationStatusAuthorizedAlways:
            case kCLAuthorizationStatusAuthorizedWhenInUse:
                [locationManager startUpdatingLocation];
                break;
            case kCLAuthorizationStatusDenied:
            case kCLAuthorizationStatusRestricted:{
                UIAlertView*alertView=[[UIAlertView alloc]initWithTitle:nil message:@"GPSサービスをオンにしてください" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                [alertView show];
                [locationManager requestWhenInUseAuthorization];
                break;}
            
            }
        NSLog(@"%d",[CLLocationManager authorizationStatus]);
    }
    // iOS7未満
    else {
        [self.locationManager startUpdatingLocation];
    }
   
    //位置情報取得間隔を指定する==distanceFilter（メートル単位が適用） kCLDistanceFilterNone==指定なし
    locationManager.distanceFilter =kCLDistanceFilterNone;
    
    //測位の制度を指定==desiredAccuracy kCLLocationAccuracyBest==最高精度
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
}

//許可状況の変更が有った場合呼ばれる
- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    
    //GPSが許可された場合　||はまたはの意味
    if (status == kCLAuthorizationStatusAuthorizedAlways ||
        status == kCLAuthorizationStatusAuthorizedWhenInUse) {
        
        // 位置測位スタート
        [locationManager startUpdatingLocation];
        
    }
}

//新しい位置情報が得られるごとに呼ばれる
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
    if (first_update_location == YES) {
        _nowLocation = [locations lastObject];
        
        //    MKCoordinateRegionを初期化　regionは中心と、表示範囲で構成
        MKCoordinateRegion region = { { 0.0, 0.0 }, { 0.0, 0.0 } };
        
        
        //regionの中心地をnewlocation.coordinateに設定
        region.center=_nowLocation.coordinate;
        
        //    //初期表示範囲を指定 latitudeDeltaなどは画面に表示する緯度経度が単位に
        region.span.latitudeDelta = 0.0067f;
        region.span.longitudeDelta = 0.0067f;
        
        [_mapView setRegion:region animated:YES];
        [locationManager stopUpdatingLocation];
        NSInteger reset=0;
        shopArray = [NSMutableArray arrayWithArray:[self requestAndParseShopList:reset]];
        
        
        ShopCollectionViewController *collectionCon = self.childViewControllers[0];
        collectionCon.shopArray = shopArray;
        
        [collectionCon.shopCollectionView reloadSections:[NSIndexSet indexSetWithIndex:0]];
        annotationArray = [NSMutableArray array];
        Pinidentifier = @"pin";
        [self pinShops];
        first_update_location = NO;
    }
    
    else return;
    
}

//位置情報取得失敗時のメソッド
-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
     UIAlertView*alertView=[[UIAlertView alloc]initWithTitle:nil message:@"位置情報取得に失敗しました" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    [alertView show];
}

#pragma mark -
#pragma mark API

- (NSArray *)requestAndParseShopList:(NSInteger)reset{
    
    
//    if(_nowLocation==nil){
//        [NSThread sleepForTimeInterval:0.5f];
//        return [self requestAndParseShopList];
//        
//    }else{
#pragma  mark ここを他からいじれるように変更する
    NSString *lat = [NSString stringWithFormat:@"%f",locationManager.location.coordinate.latitude ];
    NSString *lng = [NSString stringWithFormat:@"%f",locationManager.location.coordinate.longitude ];
    NSLog(@"%@,%@",lat,lng);
    //range=2 500m検索
    NSString *range = @"2";
    NSString *order = @"4";
    NSString *format = @"json";
    NSString *count=@"20";
    NSString *genre=@"G001";
    NSString *start;
    if(reset==0){
        start=@"1";}else if(reset==1){
        start=[NSString stringWithFormat:@"%lu",[shopArray count]+1];}
    
    NSString *url = [NSString stringWithFormat:@"http://webservice.recruit.co.jp/hotpepper/gourmet/v1/?key=850ee3e837a87b4c&format=%@&count=%@&genre=%@&lat=%@&lng=%@&range=%@&order=%@&start=%@",format,count,genre,lat,lng,range,order,start];
    
    
    
    //    リクエストを生成
    NSMutableURLRequest *request;
    request = [[NSMutableURLRequest alloc] init];
    //setHTTPMethodはGETかPOSTかのタイプ指定
    [request setHTTPMethod:@"GET"];
    //URLを設定
    [request setURL:[NSURL URLWithString:url]];
    //キャッシュ設定
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    
    [request setTimeoutInterval:20];
    [request setHTTPShouldHandleCookies:FALSE];
    
    //同期通信で送信
    NSURLResponse *response = nil;
    NSError *error = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    if (error != nil) {
        NSLog(@"Request Error:%@\n",error);
        return nil;
    }
    
    NSError *e = nil;
    
#pragma mark optionがよくわからん
    
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&e];
    NSDictionary *results = [dict objectForKey:@"results"];
    NSArray *shop = [[NSArray alloc] initWithArray: [results objectForKey:@"shop"]];
    
    return shop;
}

#pragma mark -
#pragma mark Memory

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
