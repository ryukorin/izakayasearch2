//
//  ShopCollectionViewController.h
//  IzakayaSearch
//
//  Created by TokugawaRyuichi on 11/21/14.
//  Copyright (c) 2014 HujiToku. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>

#import "ShopCell.h"
#import "EmptyCell.h"
#import "Shop.h"
#import "IzakayaSearch_Prefix.pch"
#import "MapController.h"

@interface ShopCollectionViewController : UIViewController<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UICollectionView *shopCollectionView;
@property (nonatomic, weak) NSMutableArray *shopArray;

@end
